package rabbit

import (
	"gitlab.com/some_prodject_on_microservices/service2/app"
	queueclient "gitlab.com/some_prodject_on_microservices/service2/pkg/QueueClient"
)

const NewTicketQueueName = "new-ticket"

func RegisterConsumers(qCli queueclient.QueueClient, uc app.Usecase) {
	h := NewHandler(uc)

	msgChan, err := qCli.Consume(NewTicketQueueName)
	if err != nil {
		panic(err)
	}

	for msg := range msgChan {
		h.AcceptTicket(msg)
	}
}
