package rabbit

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/streadway/amqp"
	"gitlab.com/some_prodject_on_microservices/service2/app"
	"gitlab.com/some_prodject_on_microservices/service2/models"
)

type Handler struct {
	uc app.Usecase
}

func NewHandler(uc app.Usecase) *Handler {
	return &Handler{
		uc: uc,
	}
}

func (h *Handler) AcceptTicket(msg amqp.Delivery) {
	var ticket models.Ticket

	err := json.Unmarshal(msg.Body, &ticket)
	if err != nil {
		return
	}

	err = h.uc.TickerProcessing(context.Background(), ticket)
	if err != nil {
		return
	}

	err = msg.Ack(false)
	fmt.Println(err)
}
