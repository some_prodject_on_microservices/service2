package mongo

import (
	"context"

	"gitlab.com/some_prodject_on_microservices/service2/app"
	"gitlab.com/some_prodject_on_microservices/service2/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

const (
	ticketCollection = "tickets"
)

type mongoRepo struct {
	ticketCollection *mongo.Collection
}

func NewMongoRepo(db *mongo.Database) app.Repository {
	return &mongoRepo{
		ticketCollection: db.Collection(ticketCollection),
	}
}

func (r *mongoRepo) InsertResult(ctx context.Context, ticket models.Ticket) error {
	_, err := r.ticketCollection.UpdateByID(ctx, ticket.ID, bson.D{{"$set", bson.D{{"result", ticket.Result}}}})

	return err
}
