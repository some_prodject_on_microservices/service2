package mongo

import (
	"gitlab.com/some_prodject_on_microservices/service2/models"
)

type dbTicket struct {
	ID     string `bson:"_id"`
	User   dbUser `bson:"user"`
	Result bool   `bson:"result"`
	Status int    `bson:"status"`
}

func (r *mongoRepo) toDbTicket(ticket models.Ticket) dbTicket {
	return dbTicket{
		ID:     ticket.ID.String(),
		User:   r.toDbUser(ticket.User),
		Result: ticket.Result,
		Status: int(ticket.Status),
	}
}

type dbUser struct {
	ID    string `bson:"_id"`
	Email string `bson:"email"`
}

func (r *mongoRepo) toDbUser(user models.User) dbUser {
	return dbUser{
		ID:    user.ID,
		Email: user.Email,
	}
}
