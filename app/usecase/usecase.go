package usecase

import (
	"context"
	"crypto/rand"
	"encoding/json"
	"fmt"
	"math/big"
	"time"

	"gitlab.com/some_prodject_on_microservices/service2/app"
	"gitlab.com/some_prodject_on_microservices/service2/models"
	queueclient "gitlab.com/some_prodject_on_microservices/service2/pkg/QueueClient"
)

const NotifyQueueName = "notify"

type usecase struct {
	qCli queueclient.QueueClient
	repo app.Repository
}

func NewUsecase(qCli queueclient.QueueClient, repo app.Repository) app.Usecase {
	return &usecase{
		qCli: qCli,
		repo: repo,
	}
}

func (u *usecase) TickerProcessing(ctx context.Context, ticket models.Ticket) error {
	time.Sleep(1 * time.Second)

	res, err := rand.Int(rand.Reader, big.NewInt(1))
	if err != nil {
		return err
	}

	notification := models.Notification{
		NotificationType: models.Email,
	}

	if res.Int64() == 0 {
		ticket.Result = false
		notification.Text = fmt.Sprintf("Sorry, result for your ticket %s is negative", ticket.ID)
	} else {
		ticket.Result = true
		notification.Text = fmt.Sprintf("Congratulations, result for your ticket %s is positive", ticket.ID)
	}

	err = u.repo.InsertResult(ctx, ticket)
	if err != nil {
		return err
	}

	fmt.Println("ticket updated")

	data, err := json.Marshal(notification)
	if err != nil {
		return err
	}

	return u.qCli.Publish(NotifyQueueName, data)
}
