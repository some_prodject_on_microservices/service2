package app

import (
	"context"

	"gitlab.com/some_prodject_on_microservices/service2/models"
)

type Usecase interface {
	TickerProcessing(ctx context.Context, ticket models.Ticket) error
}
