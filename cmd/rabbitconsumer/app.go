package rabbitconsumer

import (
	"context"

	mongodb "github.com/DarkSoul94/dbconnectors/MongoDB"
	"gitlab.com/some_prodject_on_microservices/service2/app"
	apprabit "gitlab.com/some_prodject_on_microservices/service2/app/delivery/rabbit"
	apprepo "gitlab.com/some_prodject_on_microservices/service2/app/repo/mongo"
	appusecase "gitlab.com/some_prodject_on_microservices/service2/app/usecase"
	"gitlab.com/some_prodject_on_microservices/service2/config"
	queueclient "gitlab.com/some_prodject_on_microservices/service2/pkg/QueueClient"
	"gitlab.com/some_prodject_on_microservices/service2/pkg/QueueClient/rabbitmq"

	"github.com/streadway/amqp"
	"go.mongodb.org/mongo-driver/mongo"
)

type App struct {
	rabbitConn *amqp.Connection
	rabbitChan *amqp.Channel
	qCli       queueclient.QueueClient
	dbClient   *mongo.Client
	uc         app.Usecase
}

func NewApp(conf config.Config) *App {
	con, chn, err := rabbitmq.Connect(
		conf.RabbitLogin,
		conf.RabbitPass,
		conf.RabbitHost,
		conf.RabbitPort,
	)
	if err != nil {
		panic(err)
	}
	qCli := rabbitmq.NewRabbitClient(chn)

	db, err := mongodb.MongoDbInit(context.Background(), conf.DbHost, conf.DbPort, conf.DbName)
	if err != nil {
		panic(err)
	}

	repo := apprepo.NewMongoRepo(db)

	uc := appusecase.NewUsecase(qCli, repo)

	return &App{
		rabbitConn: con,
		rabbitChan: chn,
		qCli:       qCli,
		uc:         uc,
	}
}

func (a *App) Run() {
	apprabit.RegisterConsumers(a.qCli, a.uc)
}

func (a *App) Stop() {
	defer a.rabbitChan.Close()
	defer a.rabbitConn.Close()
}
