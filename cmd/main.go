package main

import (
	"fmt"
	"os"
	"os/signal"

	"gitlab.com/some_prodject_on_microservices/service2/cmd/rabbitconsumer"
	"gitlab.com/some_prodject_on_microservices/service2/config"
	"gitlab.com/some_prodject_on_microservices/service2/pkg/logger"
)

func main() {
	conf := config.InitConfig()
	logger.InitLogger(conf)

	apprabbit := rabbitconsumer.NewApp(conf)
	go apprabbit.Run()

	fmt.Println(
		fmt.Sprintf(
			"Service %s is running",
			conf.AppName,
		),
	)

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, os.Interrupt)

	<-quit

	apprabbit.Stop()

	fmt.Println(
		fmt.Sprintf(
			"Service %s is stopped",
			conf.AppName,
		),
	)
}
